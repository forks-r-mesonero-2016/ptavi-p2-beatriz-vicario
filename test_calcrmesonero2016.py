#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import calcrmesonero2016


class TestCalcRMesonero2016 (unittest.TestCase):

    def setUp(self):
        self.calccalls = calcrmesonero2016.CalcCalls()

    def test_count(self):
        self.calccalls.add(2, 4)
        self.calccalls.sub(2, 4)
        self.calccalls.mul(2, 4)
        self.assertEqual(self.calccalls.count, 3)
        print(self.calccalls.count)

if __name__ == '__main__':
    unittest.main()
