#!/usr/bin/python3
# -*- coding: utf-8 -*-

from calccount import Calc


class CalcCalls(Calc):
    def calls(self):
        return self.count
